DES Demo
========
A demo of a generic block-based Discrete Event Simulation (DES):

- `des.py`: The simulator implementation and some example blocks. There is a docstring at the top of the file which takes you through how it works.
- `robot_model.py`: Shows how to build a DES model using a robot charging station as an example.
- `robot_demo.py`: Shows how to use the model, send it to the simulator, and get some results.
- `robot_demo_hist.py`: A marginally more sophisticated demo, showing how it might be used in experimentation (needs matplotlib installed).
