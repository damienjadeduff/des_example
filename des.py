"""
This is sample code for a generic block-based Discrete Event Simulator (DES).
Only a few blocks and simple statistics are implemented.
Only simple string representations are provided.
Error checking code is minimal.
Efficiency is not prioritised.
It should be enough for a student of the subject to get started.
This is based fairly loosely on the GPSS philosophy.

For more information about DES:
        Banks, Carson, Nelson & Nicol (2009). Discrete Event Simulation. Pearson: Essex, UK. ISBN-13: 978-0136062127

For more information about GPSS:
        http://www.minutemansoftware.com/general_information.htm

How to read/use the below code:

    The Entity object is just used for keeping track of entities flowing
    through the system. But your blocks can save values to these generic
    objects.

    A Resource object is for keeping track of how many of a certain resource
    (e.g. tellers at a bank) are still available.

    The FutureEventList and DelayList data structures: The FutureEventList
    is a sorted list (you might prefer a heap for efficiency) - for more info
    about this see the "event-list processing method" in DES. For information
    about the DelayList see the "two phase" approach for DES.

    The state contains the simulation clock, the two event lists, and
    state_dict which is used by blocks as a place for them to keep information.

    Most of the functionality is in the Block object and its inheriting
    objects. An entity will try to enter an object by calling its
    _do_check_entry method. The entity will remain on the delay list till this
    happens. If the _do_check_entry method returns true the entity will then
    call its _start_process method. This will generally add the entity to the
    future event list to call the block's _end_process method which is
    responsible for adding the entity to the delay list method for the
    successor block. For more details of this process see the simulate()
    method.

    Blocks that are implemented include:
        Block: Just passes the entity on to the successor block.
        GenerateBlockUniformDistribution: Generates new entities with inter-
           arrival time distributed uniformly. See GENERATE in GPSS.
        TerminateBlock: takes the entity out of the system, optionally
           terminates the simulation when N entities have left. See TERMINATE
           in GPSS.
        SeizeBlock: Allows an entity to attempt to claim a resource - the
           entity will stay in the delay list till the resource is available.
           See SEIZE and ENTER in GPSS.
        ReleaseBlock: The entity releases the resource. See RELEASE and LEAVE
           in GPSS.
        AdvanceBlockUniformDistribution: The entity stays on the block for
           a random uniformly distributed amount of time. See ADVANCE in GPSS.
        RandomTransfer: The block to which the entity transfers after this
           block is selected randomly. See TRANSFER in GPSS.

    Some blocks are purely for collecting statistics:
        StatisticsCollectionBlockTravelTimeStart
        StatisticsCollectionBlockTravelTimeEnd
            These keep a running average of the amount of time an entity spends
            travelling between the blocks. See QUEUE and DEPART in GPSS.

    Some things that are not implemented but you could implement:
        User lists (LINK and UNLINK in GPSS).
        Displacing entities from a different process (DISPLACE in GPSS).
        Spltting entities into two (SPLIT in GPSS)
        Saving values to blocks or entities or the state and doing calculations
        on them (SAVEVALUE, ASSIGN, expressions).
        Other input distributions than the uniform distribution.

        Have fun.
"""


from random import random
import logging


class Entity:
    pass


class Resource:
    def __init__(self, num_available, name='default'):
        self.reset_num_available = num_available
        self.name = name

    def __repr__(self):
        return f"<Resource({self.num_available},{self.name})>"


class FutureEventList:

    def __init__(self):
        self.fel = []

    def add_event(self, event_time, block, entity):
        self.fel.append((event_time, block, entity))
        self.fel.sort(key=lambda x: -x[0])

    def pop_next(self):
        pop = self.fel[-1]
        del self.fel[-1]
        return pop

    def __len__(self):
        return len(self.fel)

    def __repr__(self):
        return f"<FEL: {self.fel}>"


class DelayList:

    def __init__(self):
        self.dl = []

    def add_event(self, block, entity):
        self.dl.append((block, entity))

    def pop_next(self, state):
        for i, event in enumerate(self.dl):
            block = event[0]
            entity = event[1]
            if block._do_check_entry(entity=entity, state=state):
                del[self.dl[i]]
                return event

    def __len__(self):
        return len(self.dl)

    def __repr__(self):
        return f"<DL: {self.dl}>"


class State:

    def __init__(self):
        self.clock = 0
        self.fel = FutureEventList()
        self.dl = DelayList()
        self.state_dict = dict()

    def __repr__(self):
        return f"""<State clock={self.clock:0.3f}
      self.fel={self.fel}
      self.dl={self.dl}
      self.state_dict={self.state_dict}"""

# The superclass of blocks is not an abstract class:
#  in order to provide decent default functionality we set it up so that
#  transactions would simply pass straight through.


class Block:

    # In Python this is how a constructor is written.
    def __init__(self):
        logging.debug(f"Constructing block {self}.")
        self.target = None
        self.initialised = False

    # The target is the block the transaction will pass to next
    def set_target(self, target):
        logging.debug(f"Setting target of block {self} to {target}.")
        self.target = target

    # When the simulation starts this function will get called
    def _on_sim_start(self, state):
        logging.debug(f"Initialising block {self}.")
        self.initialised = True

    # Items on the delay list for this block call this function - if it
    # returns true they are processed (see the next method).
    # The method can also have side effects.
    def _do_check_entry(self, entity, state):
        logging.debug(
            f"{state.clock:.3f}: Passing entity {entity} into block {self}.")
        # Don't block
        return True

    # This starts the processing of the block - by default we simply
    # put an event for the current clock time on to the future event list to
    # finish processing.
    def _start_process(self, entity, state):
        # Process instantaneously
        state.fel.add_event(state.clock, self, entity)

    # This happens when a process finishes processing. By default we simply put
    # the transaction on the delay list of the next block.
    def _end_process(self, entity, state):
        self._process_transitions(entity=entity, state=state)

    def _process_transitions(self, entity, state):
        if self.target is not None:
            logging.debug(
                f"{state.clock:.3f}: Entity {entity} transitioning to block {self.target}.")
            state.dl.add_event(block=self.target, entity=entity)


# Let's subclass that Block class and get a Generator Block - let's do a
# special case: a block that generates entities with a uniformly distributed
# inter-arrival time
class GenerateBlockUniformDistribution(Block):

    # The construcctor just saves the inter-arrival times (also it calls the
    # super-class constructor)
    def __init__(self, min_interarrival_time, max_interarrival_time):
        super().__init__()  # call the super-class constructor
        self.min_interarrival_time = min_interarrival_time
        self.max_interarrival_time = max_interarrival_time

    # As this is a generator block we create the first entity when the simulation is started
    # It goes on the delay list which, by default, does nothing and the entity
    # goes on to _on_sim_start.
    def _on_sim_start(self, state):
        super()._on_sim_start(state)
        entity = Entity()
        self._start_process(entity=entity, state=state)
        # state.dl.add_event(entity,self)

    # The newly generated entity goes on the future event list for leaving the
    # generate block.
    def _start_process(self, entity, state):
        inter_arrival_time = random() * (self.max_interarrival_time -
                                         self.min_interarrival_time) + self.min_interarrival_time
        spawn_time = state.clock + inter_arrival_time
        state.fel.add_event(event_time=spawn_time, block=self, entity=entity)
        logging.debug(
            f"{state.clock:.3f}: Entity {entity} will spawn at {spawn_time}.")

    # When the entity is ready to leave the generate block we pass it on to the next block's delay list
    # and also create a new entity.
    def _end_process(self, entity, state):
        entity = Entity()
        state.dl.add_event(block=self, entity=entity)
        self._process_transitions(entity=entity, state=state)


# This block is the same as the superclass block except that the
# entity does not get passed on to another block.
# Instead the block simply forgets the entity.
class TerminateBlock(Block):

    def __init__(self, terminate_cntr=None):
        super().__init__()  # call the super-class constructor
        self.cntr = 0
        self.terminate_cntr = terminate_cntr

    def _on_sim_start(self, state):
        super()._on_sim_start(state)
        self.cntr = 0

    # Terminate blocks should not have child blocks (the entities just stay
    # here)
    def set_target(self, target):
        raise NotImplementedError()

    # The entity may be garbage collected if there are no more references to
    # it.
    def _end_process(self, entity, state):
        self.cntr += 1
        logging.debug(
            f"{state.clock:.3f}: Entity {entity} terminating out of {self} - cntr:{self.cntr}/{self.terminate_cntr}.")
        pass

# This block has a condition for entities on its delay list - it only allows a transaction to be processed
# if the resource is available (the Resource object would be implemented
# separately).


class SeizeBlock(Block):

    def __init__(self, resource):
        super().__init__()  # call the super-class constructor
        self.resource = resource

    def _on_sim_start(self, state):
        super()._on_sim_start(state)
        self.resource.num_available = self.resource.reset_num_available

    def _do_check_entry(self, entity, state):
        # Don't block
        if self.resource.num_available == 0:
            logging.debug(
                f"{state.clock:.3f}: Entity {entity} could not acquire resource {self.resource}.")
            return False
        else:
            self.resource.num_available -= 1
            logging.debug(
                f"{state.clock:.3f}: Entity {entity} got resource {self.resource}.")
            return True

# This block simply increments the number of available resources when the entity passes into it.
# (the Resource object would be implemented separately).


class ReleaseBlock(Block):

    def __init__(self, resource):
        super().__init__()  # call the super-class constructor
        self.resource = resource

    def _do_check_entry(self, entity, state):
        self.resource.num_available += 1
        logging.debug(
            f"{state.clock:.3f}: Entity {entity} released resource {self.resource}.")
        return True


# The superclass of blocks is not an abstract class:
#  in order to provide decent default functionality we set it up so that
#  transactions would simply pass straight through.
class AdvanceBlockUniformDistribution(Block):

    # In Python this is how a constructor is written.
    def __init__(self, min_advance, max_advance):
        super().__init__()  # call the super-class constructor
        self.min_advance = min_advance
        self.max_advance = max_advance

    # The newly generated entity goes on the future event list for leaving the
    # generate block.
    def _start_process(self, entity, state):
        advance_time = random() * (self.max_advance - self.min_advance) + self.min_advance
        finish_time = state.clock + advance_time
        state.fel.add_event(event_time=finish_time, block=self, entity=entity)
        logging.debug(
            f"{state.clock:.3f}: Entity {entity} scheduled to exit advance block {self} at {finish_time}.")


class RandomTransfer(Block):

    def __init__(self):
        super().__init__()
        self.targets = dict()

    def set_target(self, target, prob):
        self.targets[prob] = target
        assert sum(self.targets.keys()) <= 1.0
        assert sum(self.targets.keys()) >= 0.0

    def _on_sim_start(self, state):
        super()._on_sim_start(state)
        assert sum(self.targets.keys()
                   ) >= 1.0, "transition probs should add to 1"

    def _end_process(self, entity, state):
        r = random()
        cum = 0
        for prob, target in self.targets.items():
            cum += prob
            if r < cum:
                logging.debug(
                    f"{state.clock:.3f}: Entity {entity} randomly transitioning to block {target}.")
                state.dl.add_event(block=target, entity=entity)
                return
        assert False, "should not get here"


class StatisticsCollectionBlockTravelTimeStart(Block):

    def _on_sim_start(self, state):
        super()._on_sim_start(state)
        # I'll use the current block to find the stats from the state
        state.state_dict[self] = dict()

    def _end_process(self, entity, state):
        super()._end_process(entity, state)
        state.state_dict[self][entity] = state.clock


class StatisticsCollectionBlockTravelTimeEnd(Block):

    def __init__(self, start_block):
        super().__init__()
        self.start_block = start_block

    def _on_sim_start(self, state):
        super()._on_sim_start(state)
        self.avg_travel_time = 0.0
        self.num_processed_entities = 0

    def _end_process(self, entity, state):
        super()._end_process(entity, state)

        start_time = state.state_dict[self.start_block][entity]

        del state.state_dict[self.start_block][entity]

        travel_time = state.clock - start_time

        # this is how you keep a running average
        self.avg_travel_time = self.avg_travel_time * (self.num_processed_entities / (
            self.num_processed_entities + 1)) + travel_time / (self.num_processed_entities + 1)
        self.num_processed_entities += 1


def simulate(blocks, interactive=False):

    state = State()
    for block in blocks:
        block._on_sim_start(state)
    terminate = False

    while len(state.fel) > 0 and not terminate:

        # a debug idea: if len(state.dl)>20:interactive=True

        event = state.fel.pop_next()

        if interactive:
            print(f"\nFEL Simulation loop. State:\n{state}")
            print(f"\nEvent:\n{event}")
            input('Press enter to continue.')

        event_time, block, entity = event

        state.clock = event_time
        assert block.initialised, f"You did not initialise a block {block} - pass it in to the simulate function"
        block._end_process(entity=entity, state=state)

        while True:

            for block in blocks:
                if isinstance(
                        block, TerminateBlock) and block.terminate_cntr is not None and block.cntr >= block.terminate_cntr:
                    terminate = True

            if terminate:
                break

            event = state.dl.pop_next(state=state)
            if event is None:
                break

            if interactive:
                print(f"\nDL Simulation loop. State:\n{state}")
                print(f"\nEvent:\n{event}")
                input('Press enter to continue.')

            block, entity = event
            block._start_process(entity=entity, state=state)
