"""
This is where the robot model is created. Some stuff is hard-coded.
The process is self-documented which is nice.
A robot comes, queues for a charger, claims it, spends some time charging,
releases the charger, then goes. There are also blocks that the robot
travels through just for the sake of statistics collection.
"""

import des


def get_model(num_chargers, num_robots_to_proc, recharge_probability):

    charger_resource = des.Resource(num_chargers, "Charging station")

    robot_arrive = des.GenerateBlockUniformDistribution(0, 20)
    robot_queue = des.StatisticsCollectionBlockTravelTimeStart()
    robot_seize_charger = des.SeizeBlock(charger_resource)
    robot_unqueue = des.StatisticsCollectionBlockTravelTimeEnd(robot_queue)
    robot_charge = des.AdvanceBlockUniformDistribution(5, 15)
    robot_recharge_decision = des.RandomTransfer()
    robot_release_charger = des.ReleaseBlock(charger_resource)
    robot_terminate = des.TerminateBlock(num_robots_to_proc)

    robot_arrive.set_target(robot_queue)
    robot_queue.set_target(robot_seize_charger)
    robot_seize_charger.set_target(robot_unqueue)
    robot_unqueue.set_target(robot_charge)
    robot_charge.set_target(robot_recharge_decision)
    robot_recharge_decision.set_target(robot_charge, recharge_probability)
    robot_recharge_decision.set_target(
        robot_release_charger,
        1 - recharge_probability)
    robot_release_charger.set_target(robot_terminate)

    blocks = [
        robot_arrive,
        robot_queue,
        robot_seize_charger,
        robot_unqueue,
        robot_charge,
        robot_recharge_decision,
        robot_release_charger,
        robot_terminate]

    return blocks, robot_unqueue
