"""
This is a sample application for the sample implementation of a block-based
Discrete Event Simulator (DES) simulating a robot charging station in an
imaginary warehouse.

It makes a histogram out of the average queuing time of 10 robots.
"""

import des
import robot_model
import logging
from matplotlib import pyplot as plt

logging.basicConfig(level=logging.INFO)

num_chargers = 1
num_robots_to_proc = 10
recharge_probability = 0.05
num_runs = 10000

blocks, robot_unqueue = robot_model.get_model(
    num_chargers=num_chargers,
    num_robots_to_proc=num_robots_to_proc,
    recharge_probability=recharge_probability
)


queue_times = []

for runno in range(num_runs):
    if runno % 100 == 0:
        print(f"{runno}/{num_runs}")
    des.simulate(blocks, interactive=False)
    queue_times.append(robot_unqueue.avg_travel_time)


plt.hist(queue_times, bins=30)
plt.title(
    f"Histogram of average queuing times in robot charging example with {num_chargers} chargers and {num_robots_to_proc} robots queuing and arecharge probability of {recharge_probability}. N={num_runs}.")
plt.xlabel('Average Queueing Time (min)')
plt.ylabel('Frequency')
plt.show()
