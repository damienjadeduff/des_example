"""
This is a sample application for the sample implementation of a block-based
Discrete Event Simulator (DES) simulating a robot charging station in an
imaginary warehouse.

It runs the simulation once. For more detailed output, set the *interactive*
flag or change the logging level to *logging.DEBUG*.

"""
import des
import robot_model
import logging

# change the following two lines for more detailed outputs
interactive = False
logging.basicConfig(level=logging.INFO)
# logging.basicConfig(level=logging.DEBUG)

num_chargers = 2
num_robots_to_proc = 10
recharge_probability = 0.05

blocks, robot_unqueue = robot_model.get_model(
    num_chargers=num_chargers,
    num_robots_to_proc=num_robots_to_proc,
    recharge_probability=recharge_probability
)

des.simulate(blocks, interactive=interactive)

print(f"Average queue time:{robot_unqueue.avg_travel_time}")
